const CURRENTUSER = {};

function getRequest(url,typeRequest , fun, ...arr){
    const request = new XMLHttpRequest();
    request.open(typeRequest, url);
    request.responseType = 'json';
    if(typeRequest === "POST"){
         request.setRequestHeader('Content-Type', 'application/json');
        request.send(JSON.stringify(arr[0]));
    }else{
        request.send();
    }
    request.onload = () =>{
        if(request.status >= 300){
            alert(`Error ${request.status}: ${request.statusText}`);
        }else{
            fun(request.response, arr);
        }
    }
}

function showPOSTresult(result, arr){
    console.log(result)
    if(result){
        alert("tweet published!");
    }
    addCardhandler(arr[1]);
}

function delateTweet(result){
    console.log(result)
}

//            запуск функции

getRequest('https://jsonplaceholder.typicode.com/posts', 'GET', createCardObj)



function createCardObj(jsonObj){
    const objList = jsonObj.map(item=>{
        return {
            user: item.userId,
            title:item.title,
            article:item.body,
            id:item.id
        }
    })
   getRequest('https://jsonplaceholder.typicode.com/users', 'GET' , identifyUsers , objList);
}


function identifyUsers(jsonObj, bordList){
     let cardObjList = bordList[0].map(item =>{
        return {
            userName: jsonObj[item.user - 1].username,
            name:jsonObj[item.user - 1].name,
            email:jsonObj[item.user -1].email,
            title:item.title,
            article:item.article,
            id:item.id
        }
     });
    cardObjList.forEach(item=> addCardhandler(item));
    createCurrentUser(jsonObj[0].username, jsonObj[0].name, jsonObj[0].email, jsonObj[0].id , CURRENTUSER)
}


function createCurrentUser(userName, name , mail , id , obj){
    obj.userName = userName;
    obj.name = name;
    obj.email = mail;
    obj.id= id;
}
console.log(CURRENTUSER);

class Card {
    constructor({
        userName,
        name,
        email,
        title,
        article,
        id
    }) {
        this.userName = userName;
        this.name = name;
        this.mail = email;
        this.title = title;
        this.article = article;
        this.id = id;
        this.delateTwitt = this.delateTwitt.bind(this);
    }

    render() {
        this.elem = document.createElement('div');
        let {elem} = this;
        elem.className = "card";
        elem.setAttribute(`data-tweet-id`, this.id);
        elem.innerHTML = `  <p class="user-name">
                            ${this.userName}</p>
                            <i class="fas fa-clipboard-check"></i>
                            <p class="clouse event" data-clouse>х</p>
                            <p class="clouse edit" data-edit>edit</p>
                            <p class="name ">${this.name}</p>
                            <i class="fas fa-envelope"></i><p class="mail">${this.mail}</p>
                            <p class="article-title">${this.title}</p>
                            <p class="article" contenteditable="true">${this.article}</p>
                            <div class="article-bottom flex">
                            <span class="discussions">
                               <i class="fas fa-comments"></i> 12
                               </span>
                            <span class="likes">
                               <i class="fas fa-heart"></i> 50
                            </span>
                        `;
        return elem;
    }

    delateTwitt() {
          let tweent = document.querySelector(".event");
                 tweent.addEventListener('click', function (e) {
                    e.target.closest('div').remove();
                    this.tweetID = e.target.closest('div').getAttribute('data-tweet-id');
                    getRequest(`https://jsonplaceholder.typicode.com/posts/${this.tweetID}`, 'DELETE' , delateTweet);
                });
          tweent.classList.remove('event');
        }

    editTweet(){
        let edittweet = document.querySelector('.edit')
        edittweet.addEventListener('click', function(e){
           showModalWindow(e.target);
            })
    }
    }

    class ModalWindowBord{
        constructor({userName}){
            this.userName = userName;
            this.delateModal = this.delateModal.bind(this);
        }
        render(){
            this.elem = document.createElement('div');
            let {elem} = this;
            elem.className = 'card';
            elem.innerHTML = `
                            <p class="user-name">
                            ${this.userName}
                            </p>
                            <i class="fas fa-clipboard-check"></i>
                            <p class="clouse">х</p>
                             <form id="newTwitt">
                            <label class="article-title">Title
                            <input type="text" class="article article-width" name="title" required>
                            </label>
                            <textarea type="text" class="article textarea" name="article" required>
                            </textarea>
                            <input class="header-btn sing-in" type="submit" class="fadeIn fourth" value="send">
                            </form>
                            `
            return elem;
        }

    delateModal() {
        let crossesList = document.querySelectorAll('.clouse');
        crossesList.forEach(item => {
                item.addEventListener('click', function (e) {
                    e.target.closest('div').remove();
                    document.querySelector('[ data-twit-btn]').style.display="block";
                })
            })
        }
    }

    class EditModal extends ModalWindowBord {
        consyructor(){

        }
    }

    function showModalWindow(item){
        const perentElem = item.closest('div');
        const editUserObj = {

                    userName: perentElem.querySelector(".user-name").innerHTML,
                    name:perentElem.querySelector(".name").innerHTML,
                    email:perentElem.querySelector(".mail").innerHTML,
                    title:perentElem.querySelector(".article-title").innerHTML,
                    article:perentElem.querySelector(".article").innerHTML,
                    id:perentElem.getAttribute('data-tweet-id')

        }

        const editModal = new EditModal("nema");
        const newWindow = editModal.render();
        item.closest('div').innerHTML = newWindow.innerHTML ;
        perentElem.querySelector(".article-width").value = editUserObj.title;
        perentElem.querySelector(".textarea").value = editUserObj.article;
        perentElem.querySelector(".user-name").innerHTML= editUserObj.userName;
        editModal.delateModal();
        let btn = perentElem.querySelector(".header-btn");
        btn.addEventListener('click', (e)=>{
            e.preventDefault();
            let newTitle = perentElem.querySelector(".article-width").value;
            let newArticle = perentElem.querySelector(".textarea").value;
            editUserObj.title = newTitle;
            editUserObj.article = newArticle;
            let newCard = new Card(editUserObj);
            let carRender = newCard.render();
            perentElem.innerHTML = carRender.innerHTML;
            getRequest("https://jsonplaceholder.typicode.com/posts" , 'POST', showPOSTresult)
        })
    }

    function createBord(user){
        const cartBox = document.querySelector('[data-cardBox]');
        let modal = new ModalWindowBord(user);
        cartBox.prepend(modal.render());
        modal.delateModal();
        document.querySelector('#newTwitt').addEventListener('submit', function (e) {
            e.preventDefault();
            const title = this.querySelector('[name=title]').value;
            const article = this.querySelector('[name=article]').value;
            user.title = title;
            user.article = article;

            e.target.closest('div').remove();
            document.querySelector('[ data-twit-btn]').style.display = "block";

            const body = {
                title: title,
                body: article,
                userId: user.id
            }

            getRequest("https://jsonplaceholder.typicode.com/posts" , 'POST', showPOSTresult, body , user)

        })
    }

    document.querySelector('[ data-twit-btn]').addEventListener('click', (e) => {
        e.preventDefault();
        createBord(CURRENTUSER)
        e.target.style.display="none";
    });


    function addCardhandler(obj) {
        const cartBox = document.querySelector('[data-cardBox]');
        let newCard = new Card(obj);
        let cardRender = newCard.render();
        cartBox.prepend(cardRender);
        newCard.delateTwitt();
        newCard.editTweet();
    };
