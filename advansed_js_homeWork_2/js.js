class Humburger {

    static SIZE_SMALL = {
        name: "small",
        price: 50,
        calories: 20
    };

    static SIZE_LARGE = {
        name: "BIG",
        price: 100,
        calories: 20
    };

    static STUFFING_CHEESE = {
        name: "CHEESE",
        price: 10,
        calories: 20
    }

    static STUFFING_SALAD = {
        name: "SALAD",
        price: 20,
        calories: 5
    }

    static STUFFING_POTATO = {
        name: "POTATO",
        price: 15,
        calories: 10
    }

    static TOPPING_MAYO = {
        name: "MAYO",
        price: 20,
        calories: 5
    }

    static TOPPING_SPICE = {
        name: "SPICE",
        price: 15,
        calories: 0
    }

    constructor(size, stuffing) {
        this.SIZE = size;
        this.STUFFING = stuffing;
        this.toppings = [];
            if (!size) throw new HumburgerExeption("Sorry but you have added the size which we do not to have in the menu to choose the right size.")
            if (!stuffing)throw new HumburgerExeption("Sorry but you have added the stuffing which we do not to have in the menu to choose the right stuffing");
    }

    addToping(topping) {

        if (!topping) throw new HumburgerExeption("Sorry but you have added the topping which we do not to have in the menu to choose the right topping!");

        if (this.toppings.indexOf(topping) !== -1) throw new HumburgerExeption("Sorry but you have added the same topping to choose the right topping!");

        if (this.toppings.indexOf(topping) == -1) {
            this.toppings.push(topping);
        };
    }


    removeTopping(topping) {
        if (!topping) throw new HumburgerExeption("Sorry but you try ro add the topping wich you didn't added !");

        if (this.toppings.indexOf(topping) != -1)

        {
            this.toppings.splice(this.toppings.indexOf(topping), this.toppings.indexOf(topping) + 1);
        }
    };

    get getTopping() {
        this.toppingArr = this.toppings.map((item) => {
            return item.name;
        })
        return this.toppingArr;
    }
    get Size() {
        return this.SIZE.name
    }
    get Topping() {
        return this.STUFFING.name
    }
    get calculatePrice() {
        let price = 0;
        price += this.SIZE.price;
        price += this.STUFFING.price;
        let toppingPrice = this.toppings.map((item) => {
            return item.price;
        })
        toppingPrice.forEach((item) => {
            price += item;
        })
        return price;
    }

    get calculateCalories() {
        let calories = 0;
        calories += this.SIZE.calories;
        calories += this.STUFFING.calories;
        let toppingCalories = this.toppings.map((item) => {
            return item.calories;
        })
        toppingCalories.forEach((item) => {
            calories += item;
        })
    return calories;
    }
}

class HumburgerExeption {
        constructor(messagge){
            this.messagge = messagge
        }
}

const humburger = new Humburger(Humburger.SZE_SMALL, Humburger.STUFFING_CHEESE);
console.log(humburger);
humburger.addToping(Humburger.TOPPING_MAYO);
humburger.addToping(Humburger.TOPPING_SPICE);
humburger.removeTopping(Humburger.TOPPING_MAYO);
console.log(humburger.getTopping);
console.log(humburger.Size);
console.log(humburger.Topping);
console.log("Price: %f", humburger.calculatePrice, "$");
console.log("Calories: %f", humburger.calculateCalories, "cal");

try{
    const humburger = new Humburger();
}catch(err){
    console.log(err.message)
}
