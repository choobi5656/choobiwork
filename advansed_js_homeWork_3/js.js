const objAdjustment = {
    startBtn:'start',
    pouse:"pouse",
    refresh:"refresh"
}

class Game {
    constructor(){
    this.currentCell = null;
    this.gameIteration = this.gameIteration.bind(this);
    this.handlelUserClick = this.handlelUserClick.bind(this);
    this.getResult = this.getResult.bind(this);
    this.interval = null;
    this.computerInt = null;
    this.userStep = 0;
    this.botStep = 0;
    this.Spead = 0;
    this.stop = false;
    }
    getSpead(){
    this.radioBatton = document.querySelectorAll(".radio");
    for(let i = 0; i < this.radioBatton.length; i++){
        if(this.radioBatton[i].checked){
        let nabmer = this.radioBatton[i].value;
        this.Spead = Number(nabmer);
    }
    }
    if(this.Spead > 100){
        return true;
    }
    }
    getCell(){
        let cell = document.querySelectorAll("td:not(.red):not(.green):not(.blue)");

        let cellNumber = Math.floor(Math.random() * cell.length);

        if(cell.length == 0){
            if(confirm('restert')){
                this.clearInt();
                this.refreshItn();
            }
        }
       return this.currentCell = cell[cellNumber];
    }


    int(){
        this.stop = false;
        if(this.getSpead()){
            this.interval = setTimeout(this.gameIteration, this.Spead);
        }
    }

    clearInt(){
          clearInterval(this.interval);
          clearInterval(this.computerInt);
    }

    gameIteration(){
        this.getCell();
        this.currentCell.classList.add("blue");
        this.computerInt = setTimeout(()=>{
            if(this.currentCell.classList.contains("blue"))
            this.currentCell.classList.remove("blue");
            this.currentCell.classList.add("red");
            if(!this.currentCell.classList.contains('green')){
            this.handleBotStep();
            }
            if(this.stop === false){

            this.int();
            }
        }, this.Spead + 500);
         this.currentCell.addEventListener("click", this.handlelUserClick.bind(this));

    }
    refreshItn(){

        let allCell = document.querySelectorAll(".red");
        for(let i = 0; i < allCell.length; i++){
            allCell[i].classList.remove('blue');
            allCell[i].classList.remove('red');
        }

        allCell = document.querySelectorAll(".blue");
        for(let i = 0; i < allCell.length; i++){
            allCell[i].classList.remove('blue');
            allCell[i].classList.remove('red');
        }

        allCell = document.querySelectorAll(".green");
        for(let i = 0; i < allCell.length; i++){
            allCell[i].classList.remove('green');
        }

        this.clearInt();
        clearInterval(this.computerInt);
        this.botStep = 0;

        this.userStep = 0;

        this.getResult(0,0);
    }

    handlelUserClick(e){
        if(e.target.classList.contains("blue")){
             e.target.classList.remove('blue');
             e.target.classList.add("green");
            this.userStep += 1;
            this.getResult(this.userStep);
           }
    }

    handleBotStep(){
        this.botStep += 1;
        this.getResult(this.botStep);
    }

    getResult(){
          this.userTable = document.getElementById("userTable");
          this.userTable.innerHTML =  +this.userStep + '';
          this.botTable = document.getElementById("computer");
          this.botTable.innerHTML = this.botStep + "";
          if(this.userStep == 20){
              if(confirm('The winner is the user,  press ok for going back.')){
                      this.refreshItn();
              }
          }
          if(this.botStep == 20){
              if(confirm('The winner is the computer,  press ok for going back.')){
                  this.refreshItn();
                  this.stop = true;
              }
          }
      };

    getInit(){

        this.startBtn = document.getElementById("start");
        this.stopBtn = document.getElementById("pouse");
        this.refreshBtn = document.getElementById("refresh");
        this.startBtn.addEventListener("click", ()=>{
            this.int();
        });

        this.stopBtn.addEventListener("click", ()=>{
            this.clearInt();
        });

        this.refreshBtn.addEventListener("click", ()=>{
            this.refreshItn();
        });
        }
}

let game = new Game();
game.getInit();
