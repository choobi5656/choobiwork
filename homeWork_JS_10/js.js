
const passwordIcon = document.getElementById('passwordIcon');
const confirmPassword = document.getElementById('confirmPassword');
const btn = document.getElementById('button');
const lebel = document.getElementById('lebel');
const message = document.getElementById('message');
const showPasswordClasses = "fas fa-eye icon-password";
const closePasswordClasses = "fas fa-eye-slash icon-password";

passwordIcon.addEventListener('click', toggleIcons);
confirmPassword.addEventListener('click', toggleIcons);

function toggleIcons(e){
    let showIcon = e.target.className === closePasswordClasses;
    e.target.className = showIcon ? showPasswordClasses : closePasswordClasses;
    let input = e.target.previousElementSibling;
    input.setAttribute("type", showIcon ? "password" : "text");
}

btn.addEventListener("click", function(event) {
    event.preventDefault();
    let pass = document.getElementById('passCheck').value;
    let rePass = document.getElementById('passReCheck').value;
    let checkPass = pass === rePass;
    checkPass ? alert("welcome!!")(message.style.display="none") : message.style.display="block";
})
