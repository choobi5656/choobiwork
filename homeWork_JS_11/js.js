const buttons = Array.from(document.querySelectorAll(".btn-wrapper .btn"));
document.addEventListener('keydown', function(event){
    const key = event.key.toUpperCase();
    let findButton = buttons.find((item) => item.innerText.toUpperCase() === key);

//        let activeBtn = document.querySelectorAll('.btn-wrapper .active');
//        Array.from(activeBtn).map((item)=> item.classList.remove('active'));

        let activeBtn = document.querySelector('.active');
        if(findButton){
            if(activeBtn){
                activeBtn.classList.remove("active");
            }
            findButton.classList.add('active');
        };
});

