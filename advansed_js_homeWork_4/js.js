class Board {
    constructor() {
        this.columnIdCouner = 1;
        this.noteCouner = 3;
        this.allColumns = document.querySelectorAll('.column');
        this.addColumnBtn = document.getElementById('addCardBtn');
        this.collumns = document.querySelector('.columns');
    }

    addNote() {
        this.allColumns.forEach(this.addNodeFun.bind(this));
        this.contenteDitable();
        this.draggetNote();
        this.sortNots()
    }


    createBoard() {
        this.addNote();
        this.addColumnBtn.addEventListener('click', (e) => {
            e.preventDefault();
            const columnElem = document.createElement('div');
            columnElem.classList.add('column');
            columnElem.setAttribute('draggable', "true");
            this.columnIdCouner++;
            columnElem.setAttribute('data-column-id', this.columnIdCouner);
            this.noteCouner++;
            columnElem.innerHTML = `
                       <p class="column-header">To do</p><p class="dots">...</p>
                       <div  class="note-container" data-notes>
                       </div>
                       <a data-action-addnote href="#" class="columnBtn">add card <i class="fas fa-plus"></i></a>
                        `;
            this.collumns.append(columnElem);
            this.addNodeFun(columnElem);
            this.contenteDitable();
            this.draggetNote();
            this.sortNots();
        });
    }


    addNodeFun(columnElem) {
        const linkAction = columnElem.querySelector('[data-action-addnote]');
        linkAction.addEventListener('click', (e) => {
            e.preventDefault();
            this.noteCouner++;
            const noteElemnt = document.createElement('p');
            noteElemnt.classList.add('note');
            noteElemnt.setAttribute('draggable', "true");
            noteElemnt.setAttribute('data-noda-id', this.noteCouner);
            columnElem.querySelector('.note-container').append(noteElemnt);
            this.contenteDitable();
            this.draggetNote()
        });
    }


    contenteDitable() {
        let allNote = document.querySelectorAll('.note');
        let allColumnsTitle = document.querySelectorAll('.column-header');

        addContenteDitable(allNote);
        addContenteDitable(allColumnsTitle);

        function addContenteDitable(list) {
            list.forEach(function (noteElem) {
                noteElem.addEventListener('dblclick', function (e) {
                    noteElem.setAttribute('contenteditable', 'true');
                    noteElem.focus();
                });
                noteElem.addEventListener('blur', e => {
                    noteElem.removeAttribute('contenteditable');
                })
            });
        }
    }


    draggetNote() {
        let allNote = document.querySelectorAll('.note');
        allNote.forEach((Elem)=>{
            Elem.addEventListener("dragstart", dragstart_noteHandler);
            Elem.addEventListener("dragend", dragend_noteHandler);
            Elem.addEventListener("dragenter", dragenter_noteHandler);
            Elem.addEventListener("dragover", dragover_noteHandler);
            Elem.addEventListener("dragleave", dragleave_noteHandler);
            Elem.addEventListener("drop", drop_noteHandler);
        });

        let draggetNote = null;


        function dragstart_noteHandler(event) {
            draggetNote = this;
            event.stopPropagation();
            draggetNote.classList.add('dragget');
        }

        function dragend_noteHandler(event) {
            draggetNote.classList.remove('dragget');
            draggetNote = null;
            document
                .querySelectorAll('.note')
                .forEach(x => x.classList.remove('under'));
        }

        function dragenter_noteHandler(event) {
            if (this === draggetNote) {
                return
            }
            this.classList.add('under')
        }

        function dragover_noteHandler(event) {
            event.preventDefault();
            if (this === draggetNote) {
                return
            }
        }

        function dragleave_noteHandler(event) {
            if (this === draggetNote) {
                return
            }
            this.classList.remove('under')
        }

        function drop_noteHandler(event) {
            event.stopPropagation();
            if (this === draggetNote) {
                return
            }
            if(this.parentElement === draggetNote.parentElement){
                const allNote = Array.from(this.parentElement.querySelectorAll('.note'));
                const indexA = allNote.indexOf(this);
                const indexB = allNote.indexOf(draggetNote);
                if(indexA < indexB){
                    this.parentElement.insertBefore(draggetNote, this);
                }else{
                    this.parentElement.insertBefore(draggetNote, this.nextElementSibling);
                }
                }else{
                    this.parentElement.insertBefore(draggetNote, this);
                }
        }

    }

    sortNots(){
         const dots = document.querySelectorAll('.dots')
         dots.forEach((elem)=>{
             elem.addEventListener('click', function(e){

                     const box = this.nextElementSibling;
                     let notes =  Array.from(this.nextElementSibling.children);
                       let content = notes.map(item => item.innerHTML);

                         content.sort();
                         let sortedNotes = content.map(item =>{
                            return `<p class="note" draggable="true" data-noda-id="1">${item}</p>`
                        }).join(' ');

                         box.innerHTML= sortedNotes;
                         let strenchTeg = document.querySelectorAll('.lt-highlighter__wrapper');
                          for(let i = 0; i < strenchTeg.length; i++){
                             strenchTeg[i].parentElement.remove();
                         }
             })
         })
    }

}

const column = new Board();
column.createBoard();

