const btn = document.getElementById('btn');
const list = document.getElementById('list');
const lines = document.getElementById('lines');
const close = document.getElementById('close');
btn.addEventListener('click', function(e){
    e.preventDefault();
    list.classList.toggle('active');
    lines.classList.toggle('hide');
    close.classList.toggle('active');
});
