


function createNewUser(){
    let name = prompt("Enter your first Name", '');
    let lastName = prompt("Enter your second Name", '');
    let birthDay = prompt("Enter your date of birth in format", 'dd.mm.yyyy');
        let newUser ={
            userName:name,
            userSecondname:lastName,
            userDate:birthDay,
            getLogin:function(){
                return this.userName[0].toUpperCase() + this.userSecondname.toLowerCase();
            },
            getPassword:function(){
                return this.userName[0].toUpperCase() + this.userSecondname.toLowerCase() + this.userDate.slice(6);
            },
            getAge:function(){
                let dateNow = new Date();
                let month = this.userDate.slice(3,5) - 1;
                let day = this.userDate.slice(0,2);
                let age = dateNow.getFullYear() - this.userDate.slice(6);
                if(month > dateNow.getMonth() || month === dateNow.getMonth() && day > dateNow.getDate()){
                    age--;
                }
                return age;
            }
        };
        Object.defineProperties(newUser, {
             userName: {writable:false},
             userSecondname: {writable:false}
        });
    return newUser;
}

let username = createNewUser();
console.log(username.getLogin());
console.log(username.getPassword());
console.log(username.getAge());
console.log(Object.getOwnPropertyDescriptor(username, "userName"));

