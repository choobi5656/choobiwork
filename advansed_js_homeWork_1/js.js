
    Humburger.SIZE_SMALL={
        name:"small",
        price:50,
        calories:20
    };

    Humburger.SIZE_LARGE={
        name:"BIG",
        price:100,
        calories:20
    }

    Humburger.STUFFING_CHEESE = {
        name:"CHEESE",
        price:10,
        calories:20
    }

    Humburger.STUFFING_SALAD ={
        name:"SALAD",
        price:20,
        calories:5
    }

    Humburger.STUFFING_POTATO = {
        name:"POTATO",
        price:15,
        calories:10
    }

    Humburger.TOPPING_MAYO={
        name:"MAYO",
        price:20,
        calories:5
    }

    Humburger.TOPPING_SPICE = {
        name:"SPICE",
        price:15,
        calories:0
}

function Humburger(size, stuffing){
     this.SIZE = size;
     this.STUFFING = stuffing;
     this.toppings = [];
     if (!size) throw new HumburgerExeption("Sorry but you have added the size which we do not to have in the menu to choose the right size.");
     if (!stuffing) throw new HumburgerExeption("Sorry but you have added the stuffing which we do not to have in the menu to choose the right stuffing.");

};

Humburger.prototype.addToping = function (topping) {

    if (!topping) throw new HumburgerExeption("Sorry but you have added the topping which we do not to have in the menu to choose the right topping!");

    if (this.toppings.indexOf(topping) !== -1) throw new HumburgerExeption("Sorry but you have added the same topping to choose the right topping");

    if (this.toppings.indexOf(topping) == -1) {
        this.toppings.push(topping);
    };
};


Humburger.prototype.removeTopping = function(topping){

        if (!topping) throw new HumburgerExeptionRemoveToppings("Sorry but you try to remove the topping with you do not to add to hamburger first to add the topping");

    if(this.toppings.indexOf(topping) != -1)

    {this.toppings.splice(this.toppings.indexOf(topping),this.toppings.indexOf(topping) + 1);}

};

    function HumburgerExeption(messagge) {
        this.messagge = messagge;
    }

Humburger.prototype.getTopping = function(){
     this.toppingArr = this.toppings.map((item)=>{
          return item.name;
     })
    return this.toppingArr;
};

Humburger.prototype.getSize = function (){
    return this.SIZE.name
};

Humburger.prototype.getStuffing = function(){
    return this.STUFFING.name
};

Humburger.prototype.calculatePrice = function(){
    var price = 0;
    price += this.SIZE.price;
    price += this.STUFFING.price;
    var toppingPrice = this.toppings.map((item)=>{
       return item.price;
    })
    toppingPrice.forEach((item)=>{
        price += item;
    })
    return price;
};

Humburger.prototype.calculateCalories = function(){
    var calories = 0;
    calories += this.SIZE.calories;
    calories += this.STUFFING.calories;
    var toppingCalories = this.toppings.map((item)=>{
       return item.calories;
    })
    toppingCalories.forEach((item)=>{
        calories += item;
    })
    return calories;
};

var humburger = new Humburger(Humburger.SIZE_LARGE, Humburger.STUFFING_CHEESE);
console.log(humburger);
humburger.addToping(Humburger.TOPPING_MAYO);
humburger.addToping(Humburger.TOPPING_SPICE);
humburger.removeTopping(Humburger.TOPPING_MAYO);
console.log(humburger.getTopping());
console.log(humburger.getSize());
console.log(humburger.getStuffing());
console.log("Price: %f", humburger.calculatePrice(), "$");
console.log("Calories: %f", humburger.calculateCalories(), "cal");

try{
    var humburger = new Humburger();
}catch(e){
    console.log(e.message);
}
