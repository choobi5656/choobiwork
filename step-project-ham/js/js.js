$(document).ready(function(){
    (function servisesTabs(){
        $('.services_item-tabs').on("click", function(){
             let $this = $(this);
            $this.addClass('active')
                .siblings()
                .removeClass('active')
                .closest('.container')
                .find('.services_articles-item')
                .removeClass('active')
                .eq($this.index()).addClass('active');
        });
    }());

    (function workfilter(){
        $('.work_link').on('click', function(e){
            e.preventDefault();
        });
        $('.first_item').addClass('.active')
        let counter = 0;
        if($('.first_item').hasClass('.active')){
            $('.workImg_list-item').hide().slice(0,12).show();
            $('#work_btn').on('click', function(e){
            e.preventDefault();
            let $this = $(this);
            $this.hide();
            if(counter == 0){
            $('#loader').fadeIn();
            setTimeout(function(){
                $('#loader').fadeOut(3001);
                $('.workImg_list-item').slice(0,23).fadeIn(2000);
                counter++;
                setTimeout(function(){
                $this.fadeIn();
                },3000);
            }, 2000);
            }else if(counter == 1){
            $this.hide();
            $('#loader').fadeIn();
            setTimeout(function(){
                $('#loader').fadeOut();
                $('.workImg_list-item').slice(0,36).fadeIn(2000);
            }, 2000);
            conuter = 0;
            }
        });
        };
        $('.work_item').on('click', function(){
            counter = 0;
            console.log(counter);
            const $filter = $(this).data('filter');
            console.log($filter);
            $(this).addClass('active')
                .siblings()
                .removeClass('active');
                $('.workImg_list-all').find('.workImg_list-item').hide()
                .siblings($filter)
                .slice(0,12)
                .show();
                if($filter === '*'){
                    $('#work_btn').fadeIn();
                }
                else{
                    $('#work_btn').fadeOut();
                }
        });
    }());

//                    slick-slider

//    (function(){
//        $('#slider').slick({
//        slidesToShow: 1,
//          slidesToScroll: 1,
//          arrows: false,
//          fade: true,
//            asNavFor: '#slider-nav'
//            });
//        $('#slider-nav').slick({
//            slidesToShow: 3,
//            slidesToScroll: 1,
//            asNavFor: '#slider',
//            dots: false,
//            centerMode: true,
//            focusOnSelect: true,
////            appendArrows: '#arrows',
//            prevArrow: '<button id="arrow" class="slick-arrow slick-prev">&#8249</button>',
//            nextArrow: '<button id="arrow" class="slick-arrow slick-next">&#8250</button>'
//        });
//    }());
//
//    (function(){
//        $('button#arrow').on("focus", function(e){
//            let $this = $(this);
//            $('button#arrow').removeClass('active').css('outline',"none");
//            $this.addClass('active');
//        })
//    }());

//                        my-slider

    (function slider(){
        let sliderIndex = 2;
        let left = -85;
        let bottomSlides = Array.from($('#sliderBottom .slide_itemLink'));
        let topSlides = Array.from($('#sliderTop .slide_item'));
        showeSlider(sliderIndex);
        currentSlide();
        linePosotion(left);
        prevArrow();
        nextAroow();
        function prevArrow(){
            $("#prev").on('click', function(e){
                e.preventDefault();
                showeSlider(sliderIndex += -1);
                if(sliderIndex == bottomSlides.length){
                    linePosotion(left = -641);
                }else{
                    linePosotion(left += 111);
                }
                $(this).addClass('active');
                $("#next").removeClass('active');
            });
        };
        function nextAroow(){
            $("#next").on('click', function(e){
                e.preventDefault();
                showeSlider(sliderIndex += 1);
                if(sliderIndex == 1){
                    linePosotion(left = 25);
                }else{
                    linePosotion(left -= 111);
                };
                 $(this).addClass('active');
                $("#prev").removeClass('active');
            });
        };
        function currentSlide(){
            $("#sliderBottom .slide_itemLink").on('click', function(e){
                e.preventDefault();
                let currentIndex = ($(this).index()+1);
                if(currentIndex < sliderIndex){
                    linePosotion(left += 111);
                }else if(currentIndex > sliderIndex){
                    linePosotion(left -= 111);
                }
                    showeSlider(sliderIndex = ($(this).index())+1);
            });
        };
        function linePosotion(left){
            $('#line').css("left", left);
        };
        function showeSlider(n){
            let i;
            if(n > topSlides.length){
                sliderIndex = 1;
            }
            if(n < 1){
                sliderIndex=topSlides.length;
            }
            for(i = 0; i < topSlides.length; i++){
                topSlides[i].classList.remove("active");
            }
            for(i = 0; i < bottomSlides.length; i++ ){
                bottomSlides[i].classList.remove("active");
            }
            topSlides[sliderIndex-1].classList.add('active');
            bottomSlides[sliderIndex-1].classList.add('active');
        }
    }());







//
//    //                    masonry
//
      let $masonry = $(".grid").masonry({
        itemSelector: '.masonry_greid_item',
        columnWidth: 370,
        gutter:15
    });

    $masonry.imagesLoaded().progress( function() {
    $masonry.masonry('layout');
});
    //              masonry-loader

        $('.grid div').hide().slice(0,8).show();
        $('#work_btn_masonry').on('click', function(e){
            e.preventDefault();
            $('#work_btn_masonry').hide();
            $('#loader_masonry').fadeIn();
            setTimeout(function(){
            $('#loader_masonry').fadeOut();
            $('.grid div').fadeIn();
             $masonry.masonry('layout');
            },3000);

        });

});

