function getRequest(url){
const request = new XMLHttpRequest();
request.open('GET', url);
request.send();
request.onload = function(){
    if(request.status >= 300){
        alert(`Error ${request.status}: ${request.statusText}`);
    }else{
        return JSON.parse(request.response);
    }
//    return resalt;
}
request.onerror = function(){
    alert('Request is mot successful');
}
}



document.querySelector('.show_btn').addEventListener('click', e=>{
    e.preventDefault();
    createFilmList("https://swapi.dev/api/films" , document.querySelector('.films-list'));
})



function createFilmList (url, elem){
    console.log(getRequest(url));
//    const filmList = results.map(item=>{
//        return `<li class="filmName">${item.title}</li>`;
//    });
//    elem.innerHTML = filmList;
//    listHandler(results);
}

function listHandler(list){
    const filmList = Array.from(document.querySelectorAll(".filmName"));
    let itemIndex = null;
    filmList.forEach(item=>{
        item.addEventListener('click', function(e){
            itemIndex = filmList.indexOf(this);
            createFilmDescript(list, itemIndex);
        })
    })
};

function createFilmDescript(arr, index){
       const title =  document.querySelector('[data-title]');
        title.innerHTML = '';
        title.innerHTML = `${arr[index].title},     episode ${index + 1}`;
        let charactersLinkList = arr[index].characters;
        document.querySelector('[data-titleCaracters]').textContent = 'Characters';
        document.querySelector('.charactersList').innerHTML="";
        charactersLinkList.forEach(item=>{
         getRequest(item,createCharactersList,document.querySelector('.charactersList'));
        })
        document.querySelector('[data-title-id]').textContent=`episode_id ${arr[index].episode_id}`;
        document.querySelector('[data-text]').textContent = `${arr[index].opening_crawl}`;

}

function createCharactersList (result, elem){
    let item = document.createElement('li');
    item.textContent= (result.name);
    elem.append(item);
}
